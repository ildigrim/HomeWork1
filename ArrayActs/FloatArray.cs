﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayActs
{
    class FloatArray : PrimeArray
    {
        float[] array;
        public new float[] Array
        {
            get
            {
                return array;
            }
        }

        float min;
        public new float Min
        {
            get
            {
                return min;
            }
        }

        float max;
        public new float Max
        {
            get
            {
                return max;
            }
        }

        float summ;
        public new float Summ
        {
            get
            {
                return summ;
            }
        }

        public new void MinCalc()
        {
            min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                min = min <= array[i] ? min : array[i];
            }
        }

        public new void MaxCalc()
        {
            max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                max = max >= array[i] ? max : array[i];
            }
        }

        public new void SummCalc()
        {
            summ = 0;
            for (int i = 0; i < array.Length; i++)
            {
                summ += array[i];
            }
        }

        public float[] SortUp(float[] array)
        {
            float rez = 0;
            bool endFlag = true;
            do
            {
                endFlag = false;
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        rez = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = rez;
                        endFlag = true;
                    }
                }

            } while (endFlag);
            return array;
        }

        public float[] SortDown(float[] array)
        {
            float rez = 0;
            bool endFlag = true;
            do
            {
                endFlag = false;
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i] < array[i + 1])
                    {
                        rez = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = rez;
                        endFlag = true;
                    }
                }

            } while (endFlag);
            return array;
        }

        public void PrintArray(float[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
        }

        public FloatArray() { }
        public FloatArray(int[] arr, int min, int max)
        {
            float[] array = new float[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                try
                {
                    if (min == 0)
                    {
                        throw new DivideByZeroException("Минимальный элемент - 0. Невозможно создать дробный массив.");
                    }
                    array[i] = ((float)arr[i] + (float)max) / (float)min;
                }
                catch (DivideByZeroException e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                    Console.ReadKey();
                    break;
                }
            }
            this.array = array;
        }

    }
}
