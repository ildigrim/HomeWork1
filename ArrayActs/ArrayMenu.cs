﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayActs
{
    class ArrayMenu
    {
        public static void MenuExhibit()
        {
            Console.WriteLine("\nДля дальнейших манипуляций выберите:");
            Console.WriteLine("0 - нахождение минимального элемента");
            Console.WriteLine("1 - нахождение максимального элемента");
            Console.WriteLine("2 - сумма элементов массива");
            Console.WriteLine("3 - сортировка по возрастанию");
            Console.WriteLine("4 - сортировка по убыванию");
            Console.WriteLine("5 - вывести массив на экран");
            Console.WriteLine("6 - сгенерировать дробный массив");
        }

        public static void MenuExhibit(int size)
        {
            Console.Clear();
            Console.WriteLine("Размерность массива:");
            Console.WriteLine(size);
            MenuExhibit();           
        }        
    }
}
