﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayActs
{
    class Program
    {       
        static void Main(string[] args)
        {
            int size=0;
            string switchTriger = "";
            bool trig;
            bool arraySelect = true;
            do
            {
                trig = true;
                Console.Clear();
                Console.WriteLine("Размерность массива:");                       
                try
                {
                    size = Convert.ToInt32(Console.ReadLine());
                    if (size <= 0)
                    {
                        throw new Exception("Невозможно создать массив отрицательного размера.");
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                    Console.ReadKey();
                    trig = false;                   
                }
                catch (Exception e)
                {
                    Console.WriteLine("Ошибка: " + e.Message);
                    Console.ReadKey();
                    trig = false;
                }               
            } while (!trig);
            PrimeArray primeArray = new PrimeArray(size);
            primeArray.MinCalc();
            primeArray.MaxCalc();
            primeArray.SummCalc();
            FloatArray floatArray = new FloatArray(primeArray.Array, primeArray.Min,primeArray.Max);
            floatArray.MinCalc();
            floatArray.MaxCalc();
            floatArray.SummCalc();

            ArrayMenu.MenuExhibit();
          
            for (; ; )
            {
                switchTriger = Console.ReadLine();
                switch (switchTriger)
                {                    
                    case "0":                                                             //Минимальный элемент                       
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        if (arraySelect)
                        {
                            Console.WriteLine("\nМинимальный элемент: " + primeArray.Min);
                        }
                        else
                        {
                            Console.WriteLine("\nМинимальный элемент: " + floatArray.Min);
                        }
                        break;
                    case "1":                                                             //Максимальный элемен
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        if (arraySelect)
                        {
                            Console.WriteLine("\nМаксимальный элемент: " + primeArray.Max);
                        }
                        else
                        {
                            Console.WriteLine("\nМаксимальный элемент: " + floatArray.Max);
                        }
                        break;
                    case "2":                                                             //Сумма
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        if (arraySelect)
                        {
                            Console.WriteLine("\nСумма элементов: " + primeArray.Summ);
                        }
                        else
                        {
                            Console.WriteLine("\nСумма элементов: " + floatArray.Summ);
                        }
                        break;
                    case "3":                                                             //По возрастанию
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        Console.WriteLine("\nСортированый по возрастанию массив");
                        if (arraySelect)
                        {
                            int[] up = primeArray.SortUp(primeArray.Array);
                            primeArray.PrintArray(up);
                        }
                        else
                        {
                            float[] up = floatArray.SortUp(floatArray.Array);
                            floatArray.PrintArray(up);
                        }
                        break;
                    case "4":                                                             //По убыванию
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        Console.WriteLine("\nСортированый по убыванию массив");
                        if (arraySelect)
                        {
                            int[] down = primeArray.SortDown(primeArray.Array);
                            primeArray.PrintArray(down);
                        }
                        else
                        {
                            float[] down = floatArray.SortDown(floatArray.Array);
                            floatArray.PrintArray(down);
                        }
                        break;
                    case "5":                                                             //Вывод массива
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        if (arraySelect)
                        {
                            Console.WriteLine("\nСозданный массив\n");
                            primeArray.PrintArray(primeArray.Array);
                        }
                        else
                        {
                            Console.WriteLine("\nИзменённый массив\n");
                            floatArray.PrintArray(floatArray.Array);
                        }
                        break;
                    case "6":
                        Console.WriteLine("\nРабота с дробным массивом");
                        arraySelect = !arraySelect;
                        break;
                    default:
                        ArrayMenu.MenuExhibit(primeArray.Array.Length);
                        break;
                }
            }
            
        }
    }
}
