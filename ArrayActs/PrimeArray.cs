﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayActs
{
    class PrimeArray
    {
        int[] array;
        public int[] Array
        {
            get
            {
                return array;
            }
        }

        int min;
        public int Min
        {
            get
            {
                return min;
            }
        }

        int max;
        public int Max
        {
            get
            {
                return max;
            }
        }

        int summ;
        public int Summ
        {
            get
            {
                return summ;
            }
        }
        public void MinCalc()
        {
            min = array[0];
            for(int i = 0; i < array.Length; i++)
            {
                    min = min <= array[i] ? min : array[i];
            }
        }

        
        public void MaxCalc()
        {
            max = array[0];
            for (int i = 0; i < array.Length; i++)
            {                
                    max = max >= array[i] ? max : array[i];                
            }
        }

        public void SummCalc()
        {
            summ = 0;
            for (int i = 0; i < array.Length; i++)
            {
                    summ += array[i];
            }
        }

        public int[] SortUp(int[] array)
        {
            int rez = 0;
            bool endFlag = true;
            do
            {
                endFlag = false;
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        rez = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = rez;
                        endFlag = true;
                    }
                }

            } while (endFlag);
            return array;
        }

        public int[] SortDown(int[] array)
        {
            int rez = 0;
            bool endFlag = true;
            do
            {
                endFlag = false;
                for (int i = 0; i < array.Length - 1; i++)
                {
                    if (array[i] < array[i + 1])
                    {
                        rez = array[i + 1];
                        array[i + 1] = array[i];
                        array[i] = rez;
                        endFlag = true;
                    }
                }

            } while (endFlag);
            return array;
        }

        public void PrintArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
        }

        public PrimeArray() { }

        public PrimeArray(int size)
        {            
            int[] array = new int[size];
            Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                array[i] = rnd.Next(0, 1000);
            }
            this.array = array;
        }
    }
}
